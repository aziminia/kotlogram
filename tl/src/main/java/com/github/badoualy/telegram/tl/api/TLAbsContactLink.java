package com.github.badoualy.telegram.tl.api;

import static com.github.badoualy.telegram.tl.StreamUtils.*;
import static com.github.badoualy.telegram.tl.TLObjectUtils.*;

import com.github.badoualy.telegram.tl.core.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLContactLinkContact}: contactLinkContact#0</li>
 * <li>{@link TLContactLinkHasPhone}: contactLinkHasPhone#0</li>
 * <li>{@link TLContactLinkNone}: contactLinkNone#0</li>
 * <li>{@link TLContactLinkUnknown}: contactLinkUnknown#0</li>
 * </ul>
 *
 * @author Yannick Badoual yann.badoual@gmail.com
 * @see <a href="http://github.com/badoualy/kotlogram">http://github.com/badoualy/kotlogram</a>
 */
public abstract class TLAbsContactLink extends TLObject {
    public TLAbsContactLink() {
    }
}
