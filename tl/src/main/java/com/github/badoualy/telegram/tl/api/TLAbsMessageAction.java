package com.github.badoualy.telegram.tl.api;

import static com.github.badoualy.telegram.tl.StreamUtils.*;
import static com.github.badoualy.telegram.tl.TLObjectUtils.*;

import com.github.badoualy.telegram.tl.core.TLObject;

/**
 * Abstraction level for the following constructors:
 * <ul>
 * <li>{@link TLMessageActionChannelCreate}: messageActionChannelCreate#0</li>
 * <li>{@link TLMessageActionChannelMigrateFrom}: messageActionChannelMigrateFrom#0</li>
 * <li>{@link TLMessageActionChatAddUser}: messageActionChatAddUser#0</li>
 * <li>{@link TLMessageActionChatCreate}: messageActionChatCreate#0</li>
 * <li>{@link TLMessageActionChatDeletePhoto}: messageActionChatDeletePhoto#0</li>
 * <li>{@link TLMessageActionChatDeleteUser}: messageActionChatDeleteUser#0</li>
 * <li>{@link TLMessageActionChatEditPhoto}: messageActionChatEditPhoto#0</li>
 * <li>{@link TLMessageActionChatEditTitle}: messageActionChatEditTitle#0</li>
 * <li>{@link TLMessageActionChatJoinedByLink}: messageActionChatJoinedByLink#0</li>
 * <li>{@link TLMessageActionChatMigrateTo}: messageActionChatMigrateTo#0</li>
 * <li>{@link TLMessageActionEmpty}: messageActionEmpty#0</li>
 * <li>{@link TLMessageActionGameScore}: messageActionGameScore#0</li>
 * <li>{@link TLMessageActionHistoryClear}: messageActionHistoryClear#0</li>
 * <li>{@link TLMessageActionPaymentSent}: messageActionPaymentSent#0</li>
 * <li>{@link TLMessageActionPaymentSentMe}: messageActionPaymentSentMe#0</li>
 * <li>{@link TLMessageActionPhoneCall}: messageActionPhoneCall#0</li>
 * <li>{@link TLMessageActionPinMessage}: messageActionPinMessage#0</li>
 * <li>{@link TLMessageActionScreenshotTaken}: messageActionScreenshotTaken#0</li>
 * </ul>
 *
 * @author Yannick Badoual yann.badoual@gmail.com
 * @see <a href="http://github.com/badoualy/kotlogram">http://github.com/badoualy/kotlogram</a>
 */
public abstract class TLAbsMessageAction extends TLObject {
    public TLAbsMessageAction() {
    }
}
